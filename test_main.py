from main import *

def test_add_func():
    assert add_func(2,2) == 4
    assert add_func(10,3) == 13

def test_minus_func():
    assert minus_func(4,2)== 2
    assert minus_func(3,4) == 1